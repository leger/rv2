/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "parse_way.h"
#include <iostream>
#include <osmpbf/osmfile.h>
#include <osmpbf/inode.h>
#include <osmpbf/iway.h>
#include <osmpbf/irelation.h>
#include <osmpbf/filter.h>
#include <osmpbf/primitiveblockinputadaptor.h>
#include <set>

struct srtm_tile_t
{
    int lon;
    int lat;

    srtm_tile_t(const int & lon_i, const int & lat_i) : lon(lon_i), lat(lat_i) {}
    srtm_tile_t(const srtm_tile_t & orig) : lon(orig.lon), lat(orig.lat) {}
    bool operator<(const srtm_tile_t & rhs) const
    {
        if(lon<rhs.lon)
            return true;
        if(lon>rhs.lon)
            return false;
        if(lat<rhs.lat)
            return true;
        return false;
    }

    bool operator==(const srtm_tile_t & rhs) const
    {
        return (lon == rhs.lon && lat == rhs.lat);
    }
};


void parseBlock_ways(
        osmpbf::PrimitiveBlockInputAdaptor & pbi,
        uint64_t & count,
        std::set<srtm_tile_t> & srtm)
{
		
	if (pbi.waysSize())
    {
		for (osmpbf::IWayStream way = pbi.getWayStream(); !way.isNull(); way.next())
        {
            std::string tag_highway, tag_cycleway, tag_bicycle, tag_oneway, tag_junction;
			for(uint32_t i = 0, s = way.tagsSize();  i < s; ++i)
            {
                if( way.key(i) == "highway" )
                    tag_highway = way.value(i);
                if( way.key(i) == "cycleway" )
                    tag_cycleway = way.value(i);
                if( way.key(i) == "bicycle" )
                    tag_bicycle = way.value(i);
                if( way.key(i) == "oneway" )
                    tag_oneway = way.value(i);
                if( way.key(i) == "junction" )
                    tag_junction = way.value(i);
			}

            bool we_use_it;
            way_kind_t way_kind;
            int oneway;

            parse_way(
                    we_use_it,
                    way_kind,
                    oneway,
                    tag_highway,
                    tag_cycleway,
                    tag_bicycle,
                    tag_oneway,
                    tag_junction);
    
            if(we_use_it)
            {
                for(osmpbf::IWay::RefIterator refIt(way.refBegin()), refEnd(way.refEnd()); refIt != refEnd; ++refIt)
                {
                    count++;
                }
            }
		}
	}	

	if (pbi.nodesSize())
    {
		for (osmpbf::INodeStream node = pbi.getNodeStream(); !node.isNull(); node.next())
        {
            double lon = node.lond();
            double lat = node.latd();

            int lon1 = (int)((lon*1200-3)/1200 + 360)-360;
            int lon2 = (int)((lon*1200+3)/1200 + 360)-360;
            int lat1 = (int)((lat*1200-3)/1200 + 360)-360;
            int lat2 = (int)((lat*1200+3)/1200 + 360)-360;

            srtm.insert(srtm_tile_t(lon1,lat1));
            srtm.insert(srtm_tile_t(lon1,lat2));
            srtm.insert(srtm_tile_t(lon2,lat1));
            srtm.insert(srtm_tile_t(lon2,lat2));
        }
    }
}

int main(int argc, char ** argv)
{
	if (argc < 1)
    {
		fprintf(stderr,"Usage: %s pbf\n",argv[0]);
        abort();
	}
    
    uint64_t count=0;
    std::set<srtm_tile_t> srtm;
        
    std::string inputFileName(argv[1]);


    osmpbf::OSMFileIn inFile(inputFileName, false);

    if (!inFile.open())
    {
        std::cout << "Failed to open " <<  inputFileName << std::endl;
        return -1;
    }

    osmpbf::PrimitiveBlockInputAdaptor pbi;
    while (inFile.parseNextBlock(pbi))
    {
        if (pbi.isNull())
            continue;
        parseBlock_ways(pbi, count, srtm);
    }


    for(auto srtm_it = srtm.begin();
            srtm_it != srtm.end();
            srtm_it++)
    {
        fprintf(stdout,"%i\t%i\n",srtm_it->lon,srtm_it->lat);
    }

    fprintf(stderr,"number of (way,node)s: %lu\n",count);
	return 0;
}
