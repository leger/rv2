/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include "parse_way.h"

// For parsing
std::string no_str = "no";
std::string false_str = "false";
std::string motorway_str = "motorway";
std::string trunk_str = "trunk";
std::string construction_str = "construction";
std::string steps_str = "steps";
std::string foot_str = "foot";
std::string pedestrian_str = "pedestrian";
std::string track_str = "track";
std::string m1_str = "-1";
std::string p1_str = "1";
std::string yes_str = "yes";
std::string true_str = "true";
std::string roundabout_str = "roundabout";
std::string opposite_str = "opposite";

void parse_way(
        bool& we_use_it, 
        way_kind_t& way_kind,
        int& oneway, 
        std::string& tag_highway,
        std::string& tag_cycleway,
        std::string& tag_bicycle,
        std::string& tag_oneway,
        std::string& tag_junction)
{
    we_use_it = false;

    // if the tag cycleway is present and different of no and false
    // we take the way in kind RV_WK_CYCLEWAY
    if(tag_cycleway.size()>=2 && tag_cycleway.substr(0,2) != no_str)
    {
        if(tag_cycleway.size()>=5)
        {
            if(tag_cycleway.substr(0,5) != false_str)
            {
                way_kind = RV_WK_CYCLEWAY;
                we_use_it = true;
            }
        }
        else
        {
            way_kind = RV_WK_CYCLEWAY;
            we_use_it = true;
        }
    }

    // else if the tag bicycle is present and different of no and false
    // we take the way in kind RV_WK_OTHERS
    if(!we_use_it)
    {
        if(tag_bicycle.size()>=2 && tag_bicycle.substr(0,2) != no_str)
        {
            if(tag_bicycle.size()>=5)
            {
                if(tag_bicycle.substr(0,5) != false_str)
                {
                    way_kind = RV_WK_OTHERS;
                    we_use_it = true;
                }
            }
            else
            {
                way_kind = RV_WK_OTHERS;
                we_use_it = true;
            }
        }
    }
    
    // else if the tag highway is present and begin by foot or pedestrian
    // we take the way in kind RV_WK_FOOTWAY
    if(!we_use_it)
    {
        if(tag_highway.size()>=10 && tag_highway.substr(0,10) == pedestrian_str)
        {
            way_kind = RV_WK_FOOTWAY;
            we_use_it = true;
        }
        if(tag_highway.size()>=4 && tag_highway.substr(0,4) == foot_str)
        {
            way_kind = RV_WK_FOOTWAY;
            we_use_it = true;
        }
    }
    
    
    // else if the tag highway is present and different of motorway /
    // trunk / construction / steps / track we take in in kind
    // RV_WK_OTHERS
    if(!we_use_it)
    {
        if(tag_highway.size()>0)
        {
            we_use_it = true;
            if(tag_highway.size()>=8 && tag_highway.substr(0,8) == motorway_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == trunk_str)
                we_use_it=false;
            if(tag_highway.size()>=12 && tag_highway.substr(0,12) == construction_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == steps_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == track_str)
                we_use_it=false;

            // and we check for the exception
            if(tag_bicycle.size()>=2 && tag_bicycle.substr(0,2) == no_str)
                we_use_it=false;
            if(tag_bicycle.size()>=5 && tag_bicycle.substr(0,5) == false_str)
                we_use_it=false;

            if(we_use_it)
                way_kind = RV_WK_OTHERS;
        }
    }
    
    if(we_use_it)
    {
        oneway=0;
        if(tag_oneway.size()>=2 && tag_oneway.substr(0,2) == m1_str)
            oneway=-1;
        if(tag_oneway.size()>=1 && tag_oneway.substr(0,1) == p1_str)
            oneway=1;
        if(tag_oneway.size()>=3 && tag_oneway.substr(0,3) == yes_str)
            oneway=1;
        if(tag_oneway.size()>=4 && tag_oneway.substr(0,4) == true_str)
            oneway=1;

        if(tag_junction.size()>=10 && tag_junction.substr(0,10) == roundabout_str)
            oneway=1;
        
        if(tag_cycleway.size()>=8 && tag_cycleway.substr(0,8) == opposite_str)
            oneway=0;
    }

}

