/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "nodes_db.h"

nodes_db_t::nodes_db_t(const char* db_location, bool rwi)
{
    int rc;
    rw = rwi;

    rc = mdb_env_create(&env);
    if(rc)
    {
        fprintf(stderr, "mdb_env_create: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_set_mapsize(env, RV_MAXIMUM_LMDB_SIZE);
    if(rc)
    {
        fprintf(stderr, "mdb_env_set_mapsize: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_open(env, db_location, 0, 0644);
    if(rc)
    {
        fprintf(stderr, "mdb_env_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    if(rw)
        rc = mdb_txn_begin(env, NULL, 0, &txn);
    else
        rc = mdb_txn_begin(env, NULL, MDB_RDONLY, &txn);
    if(rc)
    {
        fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_dbi_open(txn, NULL, 0, &dbi);
    if(rc)
    {
        fprintf(stderr, "mdb_dbi_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }
}

nodes_db_t::~nodes_db_t()
{
    mdb_txn_abort(txn);
    mdb_close(env, dbi);
    mdb_env_close(env);
}

void nodes_db_t::commit()
{
    if(rw)
    {
        int rc;

        rc = mdb_txn_commit(txn);
        if(rc)
        {
            fprintf(stderr, "mdb_txn_commit: (%d) %s\n", rc, mdb_strerror(rc));
            abort();
        }

        rc = mdb_txn_begin(env, NULL, 0, &txn);
        if(rc)
        {
            fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
            abort();
        }
    }
}

int nodes_db_t::get_node(nid_t & id, node_info_t* pni)
{
    MDB_val mdb_key, mdb_data;
    
    mdb_key.mv_size = sizeof(nid_t);
    mdb_key.mv_data = &id;

    int rc;
    rc = mdb_get(txn,dbi,&mdb_key,&mdb_data);
    if(rc)
    {
        if(rc == MDB_NOTFOUND)
        {
            fprintf(stderr, "node not found : %lu\n", id);
        }
        fprintf(stderr, "mdb_get: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }
    
    build_node_info_from_MDB_val(pni, &mdb_data);
    return(0);
}

int nodes_db_t::write_node(
        nid_t & id,
        double & lon,
        double & lat,
        elevation_value & elev,
        std::list<neighbor_t> & neighbors)
{
    MDB_val mdb_key, mdb_data;
    
    mdb_key.mv_size = sizeof(nid_t);
    mdb_key.mv_data = &id;

    build_MDB_val_from_node_info(&mdb_data, lon, lat, elev, neighbors);

    int rc;
    rc = mdb_put(txn, dbi, &mdb_key, &mdb_data, 0);
    if(rc)
    {
        fprintf(stderr, "mdb_put: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    free_MDB_val_from_node_info(&mdb_data);

    return(rc);
}


