/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "conv_functions.h"

void build_MDB_val_from_node_info(
        MDB_val* mdb_value,
        double & lon, 
        double & lat, 
        elevation_value & elevation, 
        std::list<neighbor_t> & neighbors
        )
{
    // Return a pointer (don't forget to free it with free()) to a
    // build_node_info with allocation of the two internal structure in the same
    // allocation. Design to be used to build a MDB_val
    char* pc;
    node_info_t ni;
    size_t len;
    len =   sizeof(node_info_fixed_t)
            +neighbors.size()*sizeof(neightbor_t);
    pc = (char*)malloc(len);
    if(!pc)
    {
        fprintf(stderr,"Memory allocation failed\n");
        abort();
    }
    
    ni.fixed = (node_info_fixed_t*)pc;
    
    if(neighbors.size()>0)
        ni.neighbors = (neighbor_t*)(pc+sizeof(node_info_fixed_t));
    else
        ni.neighbors = (neighbor_t*)NULL;

    ni.fixed->lon = lon;
    ni.fixed->lat = lat;
    ni.fixed->elevation = elevation;
    ni.fixed->neighbors_number = neighbors.size();

    unsigned int i=0;
    for(
            std::list<neighbor_t>::iterator neighbors_it = neighbors.begin();
            neighbors_it != neighbors.end();
            neighbors_it++
       )
    {
        ni.neighbors[i].from = (*neighbors_it).from;
        ni.neighbors[i].to = (*neighbors_it).to;
        ni.neighbors[i].way_kind = (*neighbors_it).way_kind;
        i++;
    }

    mdb_value->mv_size = len;
    mdb_value->mv_data = (void*)pc;
}

void free_MDB_val_from_node_info(
        MDB_val* mdb_value
        )
{
    free(mdb_value->mv_data);
    mdb_value->mv_data = (void*)NULL;
    mdb_value->mv_size = 0;
}

void build_node_info_from_MDB_val(
        node_info_t* pni,
        MDB_val* mdb_value
        )
{
    if(mdb_value->mv_size<sizeof(node_info_fixed_t))
    {
        fprintf(stderr,"Error, database node info to short\n");
        abort();
    }
    pni->fixed = (node_info_fixed_t*)(mdb_value->mv_data);
    if(mdb_value->mv_size!=sizeof(node_info_fixed_t)+(pni->fixed->neighbors_number)*sizeof(neighbor_t))
    {
        fprintf(stderr,"Error, database node info size mismatch\n");
        abort();
    }
    if(pni->fixed->neighbors_number>0)
        pni->neighbors = (neighbor_t*)(((char*)mdb_value->mv_data)+sizeof(node_info_fixed_t));
    else
        pni->neighbors = (neighbor_t*)NULL;
}







    

