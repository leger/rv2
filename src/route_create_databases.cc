/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "conv_functions.h"
#include "elevation.h"
#include "types.h"
#include "nodes_db.h"
#include "lookup_db.h"
#include <sys/stat.h>
#include <sys/types.h>


#include <pqxx/pqxx>

// For parsing
std::string no_str = "no";
std::string false_str = "false";
std::string motorway_str = "motorway";
std::string trunk_str = "trunk";
std::string construction_str = "construction";
std::string steps_str = "steps";
std::string foot_str = "foot";
std::string pedestrian_str = "pedestrian";
std::string track_str = "track";
std::string m1_str = "-1";
std::string p1_str = "1";
std::string yes_str = "yes";
std::string true_str = "true";
std::string roundabout_str = "roundabout";
std::string opposite_str = "opposite";

struct simplified_node
{
    nid_t nid;
    double lon;
    double lat;
    elevation_value elev;
};

void parse_way(
        bool& we_use_it, 
        way_kind_t& way_kind,
        int& oneway, 
        std::string& tag_highway,
        std::string& tag_cycleway,
        std::string& tag_bicycle,
        std::string& tag_oneway,
        std::string& tag_junction)
{
    we_use_it = false;

    // if the tag cycleway is present and different of no and false
    // we take the way in kind RV_WK_CYCLEWAY
    if(tag_cycleway.size()>=2 && tag_cycleway.substr(0,2) != no_str)
    {
        if(tag_cycleway.size()>=5)
        {
            if(tag_cycleway.substr(0,5) != false_str)
            {
                way_kind = RV_WK_CYCLEWAY;
                we_use_it = true;
            }
        }
        else
        {
            way_kind = RV_WK_CYCLEWAY;
            we_use_it = true;
        }
    }

    // else if the tag bicycle is present and different of no and false
    // we take the way in kind RV_WK_OTHERS
    if(!we_use_it)
    {
        if(tag_bicycle.size()>=2 && tag_bicycle.substr(0,2) != no_str)
        {
            if(tag_bicycle.size()>=5)
            {
                if(tag_bicycle.substr(0,5) != false_str)
                {
                    way_kind = RV_WK_OTHERS;
                    we_use_it = true;
                }
            }
            else
            {
                way_kind = RV_WK_OTHERS;
                we_use_it = true;
            }
        }
    }
    
    // else if the tag highway is present and begin by foot or pedestrian
    // we take the way in kind RV_WK_FOOTWAY
    if(!we_use_it)
    {
        if(tag_highway.size()>=10 && tag_highway.substr(0,10) == pedestrian_str)
        {
            way_kind = RV_WK_FOOTWAY;
            we_use_it = true;
        }
        if(tag_highway.size()>=4 && tag_highway.substr(0,4) == foot_str)
        {
            way_kind = RV_WK_FOOTWAY;
            we_use_it = true;
        }
    }
    
    
    // else if the tag highway is present and different of motorway /
    // trunk / construction / steps / track we take in in kind
    // RV_WK_OTHERS
    if(!we_use_it)
    {
        if(tag_highway.size()>0)
        {
            we_use_it = true;
            if(tag_highway.size()>=8 && tag_highway.substr(0,8) == motorway_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == trunk_str)
                we_use_it=false;
            if(tag_highway.size()>=12 && tag_highway.substr(0,12) == construction_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == steps_str)
                we_use_it=false;
            if(tag_highway.size()>=5 && tag_highway.substr(0,5) == track_str)
                we_use_it=false;

            // and we check for the exception
            if(tag_bicycle.size()>=2 && tag_bicycle.substr(0,2) == no_str)
                we_use_it=false;
            if(tag_bicycle.size()>=5 && tag_bicycle.substr(0,5) == false_str)
                we_use_it=false;

            if(we_use_it)
                way_kind = RV_WK_OTHERS;
        }
    }
    
    if(we_use_it)
    {
        oneway=0;
        if(tag_oneway.size()>=2 && tag_oneway.substr(0,2) == m1_str)
            oneway=-1;
        if(tag_oneway.size()>=1 && tag_oneway.substr(0,1) == p1_str)
            oneway=1;
        if(tag_oneway.size()>=3 && tag_oneway.substr(0,3) == yes_str)
            oneway=1;
        if(tag_oneway.size()>=4 && tag_oneway.substr(0,4) == true_str)
            oneway=1;

        if(tag_junction.size()>=10 && tag_junction.substr(0,10) == roundabout_str)
            oneway=1;
        
        if(tag_cycleway.size()>=8 && tag_cycleway.substr(0,8) == opposite_str)
            oneway=0;
    }

}

int main(int argc, char** argv)
{
    if(argc<4)
    {
        fprintf(stderr,"Usage: %s postgres_db_connexion_str elevation_db_path route_db_path\n",argv[0]);
        abort();
    }

    char zreq[RV_ZCHAR_LENGTH];
    char zfilename[RV_ZCHAR_LENGTH];

    // postgres (write temporary tables)
    pqxx::connection postgres_db(argv[1]);
    pqxx::work postgres_txn(postgres_db);

    // elevation_db (read-only)
    elevation elevation_db(argv[2]);

    // cleaning (without checking status, lmdb will check after and fail if this
    // is not goot)

    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/data.mdb",argv[3],RV_NODES_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/lock.mdb",argv[3],RV_NODES_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/data.mdb",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    remove(zfilename);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s/lock.mdb",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    remove(zfilename);
    mkdir(argv[3],0755);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_NODES_DB_RELATIVE_PATH);
    mkdir(zfilename,0755);
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    mkdir(zfilename,0755);
    
    // nodes_db (write)
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_NODES_DB_RELATIVE_PATH);
    nodes_db_t nodes_db(zfilename,true);

    // lookup_db (write)
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[3],RV_LOOKUP_DB_RELATIVE_PATH);
    lookup_db_t lookup_db(zfilename,true);

    // we lock all table we use, in case of somebody change the db. We don't
    // block read access (obviously)

    postgres_txn.exec("LOCK TABLE nodes IN EXCLUSIVE MODE");
    postgres_txn.exec("LOCK TABLE ways IN EXCLUSIVE MODE");
    postgres_txn.exec("LOCK TABLE way_nodes IN EXCLUSIVE MODE");


    // First we create a table of ways (in a scope for destruction of the
    // pipline, and some variables)
    RV_BLANKLINE;
    fprintf(stderr,"Computing ways\n");
    unsigned int total_number_of_ways;
    {
        RV_BLANKLINE;
        fprintf(stderr,"-> querying ");
        pqxx::result res_ways = postgres_txn.exec(
                "SELECT id,tags->'highway',tags->'cycleway',tags->'bicycle',tags->'oneway',tags->'junction' FROM ways");

        pqxx::pipeline postgres_txn_pipeline(postgres_txn);
        std::list<pqxx::pipeline::query_id> running;
        postgres_txn_pipeline.retain(RV_POSTGRES_PIPELINE_RETAIN);
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "CREATE TEMP TABLE %sways (id BIGINT, kind INT, cc_id BIGINT, oneway INT)",
                RV_TEMP_TABLES_PREFIX);
        postgres_txn_pipeline.insert(zreq);

        unsigned int total_number = res_ways.size();
        unsigned int i=0;
        int previous_displayed = -1;

        for(pqxx::result::iterator res_ways_it = res_ways.begin();
                res_ways_it!=res_ways.end();
                res_ways_it++)
        {
            // Probably not the most efficient. But we name fields by human
            // understable names, by affecting variables.
            wid_t wid = res_ways_it[0].as<wid_t>();
            std::string tag_highway(res_ways_it[1].c_str());
            std::string tag_cycleway(res_ways_it[2].c_str());
            std::string tag_bicycle(res_ways_it[3].c_str());
            std::string tag_oneway(res_ways_it[4].c_str());
            std::string tag_junction(res_ways_it[5].c_str());

            bool we_use_it;
            way_kind_t way_kind;
            int oneway;

            parse_way(
                    we_use_it,
                    way_kind,
                    oneway,
                    tag_highway,
                    tag_cycleway,
                    tag_bicycle,
                    tag_oneway,
                    tag_junction);

            // Now if we use it
            if(we_use_it)
            {
                if(running.size()>RV_MAX_PIPELINE)
                {
                    postgres_txn_pipeline.retrieve(running.front());
                    running.pop_front();
                }
                snprintf(zreq,RV_ZCHAR_LENGTH,
                        "INSERT INTO %sways (id,kind,cc_id,oneway) VALUES (%lu,%u,%lu,%i)",
                        RV_TEMP_TABLES_PREFIX,
                        wid,
                        (int)way_kind,
                        wid,
                        oneway);
                running.push_back(postgres_txn_pipeline.insert(zreq));
                total_number_of_ways++;
            }
            i++;
            if((int)(100*i/total_number) > previous_displayed)
            {
                previous_displayed = 100*i/total_number;
                RV_BLANKLINE;
                fprintf(stderr,"-> %i%% ",previous_displayed);
            }
        }

        RV_BLANKLINE;
        fprintf(stderr,"-> Waiting pipeline completetion ");
        postgres_txn_pipeline.complete();
    }

    // a index to have quick join
    RV_BLANKLINE;
    fprintf(stderr,"-> Adding indexes ");
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ALTER TABLE %sways ADD CONSTRAINT pk_%sways PRIMARY KEY (id)",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ANALYZE %sways",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);

    RV_BLANKLINE;
    fprintf(stderr,"Computing connected components\n");
    // Now we compute connected components
    
    // a table for way adjacency
    RV_BLANKLINE;
    fprintf(stderr,"-> Computing ways adjacency table ");
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "CREATE TEMP TABLE %sway_way (id1 BIGINT, id2 BIGINT)",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "INSERT INTO %sway_way \
                ( \
                    SELECT DISTINCT \
                            w1.id AS id1, \
                            w2.id AS id2 \
                        FROM \
                                %sways AS w1 \
                            INNER JOIN \
                                way_nodes AS wn1 ON wn1.way_id=w1.id \
                            INNER JOIN \
                                way_nodes AS wn2 ON wn2.node_id=wn1.node_id AND wn2.way_id<>wn1.way_id \
                            INNER JOIN \
                                %sways AS w2 ON w2.id=wn2.way_id \
                 )",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "INSERT INTO %sway_way \
                ( \
                    SELECT \
                            w.id AS id1, \
                            w.id AS id2 \
                        FROM \
                                %sways AS w \
                 )",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);

    // a index to have quick join
    RV_BLANKLINE;
    fprintf(stderr,"-> Adding indexes ");
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ALTER TABLE %sway_way ADD CONSTRAINT pk_%sway_way PRIMARY KEY (id1,id2)",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ANALYZE %sway_way",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
        
    unsigned int i=0;
    unsigned number_of_ways_previously_updated = total_number_of_ways;
    
    while(number_of_ways_previously_updated>0)
    {
        i++;
        RV_BLANKLINE;
        fprintf(stderr,
                "-> Extend. Pass %u. (previous pass %u/%u ways updated) ",
                i,
                number_of_ways_previously_updated,
                total_number_of_ways);

        snprintf(zreq,RV_ZCHAR_LENGTH,
            "WITH \
                mm AS \
                    ( \
                            SELECT \
                                wc1.cc_id AS old_cc_id, \
                                min(wc2.cc_id) AS new_cc_id \
                            FROM \
                                    %sways AS wc1 \
                                INNER JOIN \
                                    %sway_way AS ww ON wc1.id=ww.id1 \
                                INNER JOIN \
                                    %sways AS wc2 ON ww.id2=wc2.id \
                            GROUP BY \
                                wc1.cc_id \
                    ) \
                UPDATE %sways \
                    SET \
                        cc_id=sub.new_cc_id \
                    FROM \
                        ( \
                            SELECT * FROM mm WHERE NOT old_cc_id=new_cc_id \
                        ) AS sub \
                    WHERE \
                        cc_id=sub.old_cc_id;",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);

        pqxx::result res_propa = postgres_txn.exec(zreq);
        number_of_ways_previously_updated = res_propa.affected_rows();
    }



    // Node table
    RV_BLANKLINE;
    fprintf(stderr,"Computing nodes table\n");
    
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "CREATE TEMP TABLE %snodes (id BIGINT, cc_id BIGINT, elev FLOAT)",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);

    snprintf(zreq,RV_ZCHAR_LENGTH,
            "SELECT AddGeometryColumn('%snodes', 'geom', 4326, 'POINT', 2);",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);

    RV_BLANKLINE;
    fprintf(stderr,"-> Inserting data ");

    snprintf(zreq,RV_ZCHAR_LENGTH,
            "INSERT INTO %snodes \
                ( \
                    SELECT \
                            n.id AS id, \
                            n.cc_id AS cc_id, \
                            NULL AS elev, \
                            nodes.geom AS geom \
                        FROM \
                                ( \
                                    SELECT DISTINCT \
                                            way_nodes.node_id AS id, \
                                            %sways.cc_id AS cc_id \
                                        FROM \
                                                way_nodes \
                                            INNER JOIN \
                                                %sways ON way_nodes.way_id=%sways.id \
                                ) AS n \
                            INNER JOIN \
                                nodes ON nodes.id=n.id \
                )",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);

    unsigned int total_number_of_nodes = postgres_txn.exec(zreq).affected_rows();
    
    RV_BLANKLINE;
    fprintf(stderr,"-> Creating indexes ");
    
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ALTER TABLE %snodes ADD CONSTRAINT pk_%snodes PRIMARY KEY (id)",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    
    snprintf(zreq,RV_ZCHAR_LENGTH,
            "CREATE INDEX idx_%snodes_geom ON %snodes USING gist (geom)",
            RV_TEMP_TABLES_PREFIX,
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);

    snprintf(zreq,RV_ZCHAR_LENGTH,
            "ANALYZE %snodes",
            RV_TEMP_TABLES_PREFIX);
    postgres_txn.exec(zreq);
    
    RV_BLANKLINE;
    fprintf(stderr,"Computing elevation on nodes table\n");
    {
        fprintf(stderr,"-> Querying nodes ");
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "SELECT id,ST_X(geom),ST_Y(geom) FROM %snodes",
                RV_TEMP_TABLES_PREFIX);
        
        pqxx::result res_nodes=postgres_txn.exec(zreq);

        RV_BLANKLINE;
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "PREPARE %snodes_elev (FLOAT,BIGINT) AS UPDATE %snodes SET elev=$1 WHERE id=$2",
                RV_TEMP_TABLES_PREFIX,
                RV_TEMP_TABLES_PREFIX);
        postgres_txn.exec(zreq);
        
        unsigned int i=0;
        int previous_displayed=-1;

        pqxx::pipeline postgres_txn_pipeline(postgres_txn);
        std::list<pqxx::pipeline::query_id> running;
        postgres_txn_pipeline.retain(RV_POSTGRES_PIPELINE_RETAIN);
        for(pqxx::result::iterator res_nodes_it = res_nodes.begin();
                res_nodes_it!=res_nodes.end();
                res_nodes_it++)
        {
            if(running.size()>RV_MAX_PIPELINE)
            {
                postgres_txn_pipeline.retrieve(running.front());
                running.pop_front();
            }
            nid_t nid = res_nodes_it[0].as<nid_t>();
            double lon = res_nodes_it[1].as<double>();
            double lat = res_nodes_it[2].as<double>();
            elevation_value elev = elevation_db.get_elevation(lon,lat);
            snprintf(zreq,RV_ZCHAR_LENGTH,
                    "EXECUTE %snodes_elev(%f,%lu)",
                    RV_TEMP_TABLES_PREFIX,
                    elev,
                    nid);
            running.push_back(postgres_txn_pipeline.insert(zreq));
            
            i++;
            if((int)(100*i/total_number_of_nodes)>previous_displayed)
            {
                previous_displayed = 100*i/total_number_of_nodes;
                RV_BLANKLINE;
                fprintf(stderr,"-> %i%% ",previous_displayed);
            }
        }
        
        RV_BLANKLINE;
        fprintf(stderr,"-> Waiting pipeline completetion ");
        postgres_txn_pipeline.complete();
    }


    // Writing nodes
    
    RV_BLANKLINE;
    fprintf(stderr,"Writing nodes in nodes_db\n");
    // We prepare the request for searching neighbors, this request is used for
    // each node
    // We work in a scope to destroy the result after
    {
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "PREPARE %sget_neighbors (BIGINT) AS SELECT \
                        wn2.node_id, \
                        wn2.sequence_id-wn1.sequence_id, \
                        w.oneway, \
                        w.kind \
                    FROM  \
                            way_nodes AS wn1 \
                        INNER JOIN \
                            way_nodes AS wn2 ON wn2.way_id=wn1.way_id \
                        INNER JOIN \
                            %sways AS w ON w.id=wn1.way_id \
                    WHERE  \
                            abs(wn1.sequence_id-wn2.sequence_id)=1 \
                        AND \
                            wn1.node_id=$1",
                RV_TEMP_TABLES_PREFIX,
                RV_TEMP_TABLES_PREFIX);
        postgres_txn.exec(zreq);
        
        RV_BLANKLINE;
        fprintf(stderr,"-> querying ");
        
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "SELECT id,ST_X(geom),ST_Y(geom),elev FROM %snodes",
                RV_TEMP_TABLES_PREFIX);
        
        pqxx::result res_nodes = postgres_txn.exec(zreq);

        unsigned int i=0;
        int previous_displayed=-1;

        pqxx::pipeline postgres_txn_pipeline(postgres_txn);
        postgres_txn_pipeline.retain(RV_POSTGRES_PIPELINE_RETAIN);
        std::list< std::pair< simplified_node, pqxx::pipeline::query_id > > queries;
        

        fprintf(stderr,"\r-> begin Q ");
        for(pqxx::result::iterator res_nodes_it = res_nodes.begin();
                res_nodes_it!=res_nodes.end();
                res_nodes_it++)
        {
            std::pair< simplified_node, pqxx::pipeline::query_id > query;

            query.first.nid = res_nodes_it[0].as<nid_t>();
            query.first.lon = res_nodes_it[1].as<double>();
            query.first.lat = res_nodes_it[2].as<double>();
            query.first.elev = res_nodes_it[3].as<elevation_value>();

            snprintf(zreq,RV_ZCHAR_LENGTH,
                    "EXECUTE %sget_neighbors(%lu)",
                    RV_TEMP_TABLES_PREFIX,
                    query.first.nid);

            query.second = postgres_txn_pipeline.insert(zreq);
            queries.push_back(query);

            while(queries.size()>0)
            {
                if(queries.size()<RV_MAX_PIPELINE && (res_nodes_it + 1)!=res_nodes.end())
                    break;

                std::pair< simplified_node, pqxx::pipeline::query_id > query = queries.front();
                queries.pop_front();

        
                std::list< std::pair<nid_t,way_kind_t> > neighbors_from;
                std::list<nid_t> neighbors_to;
                nid_t nid;
                double lon,lat;
                elevation_value elev;

                nid = query.first.nid;
                lon = query.first.lon;
                lat = query.first.lat;
                elev = query.first.elev;

                pqxx::result res_neighbors = postgres_txn_pipeline.retrieve(query.second);

                for(pqxx::result::iterator res_neighbors_it = res_neighbors.begin();
                        res_neighbors_it != res_neighbors.end();
                        res_neighbors_it++)
                {
                    bool allowed_from=false;
                    bool allowed_to=false;

                    nid_t nid_neighbor = res_neighbors_it[0].as<nid_t>();
                    int diff = res_neighbors_it[1].as<int>();
                    int oneway = res_neighbors_it[2].as<int>();
                    way_kind_t way_kind = (way_kind_t)res_neighbors_it[3].as<int>();

                    if(oneway==0)
                    {
                        allowed_from=true;
                        allowed_to=true;
                    }
                    else if(oneway==1)
                    {
                        if(diff==1)
                            allowed_to=true;
                        else
                            allowed_from=true;
                    }
                    else if(oneway==-1)
                    {
                        if(diff==1)
                            allowed_from=true;
                        else
                            allowed_to=true;
                    }

                    if(allowed_from)
                        neighbors_from.push_back( std::pair<nid_t,way_kind_t>(nid_neighbor,way_kind) );
                    if(allowed_to)
                        neighbors_to.push_back( nid_neighbor );
                }
                        
                std::list<neighbor_t> neighbors;
                for(std::list< std::pair<nid_t,way_kind_t> >::iterator neighbors_from_it = neighbors_from.begin();
                        neighbors_from_it!=neighbors_from.end();
                        neighbors_from_it++)
                {
                    nid_t nid_from = (*neighbors_from_it).first;
                    way_kind_t way_kind = (*neighbors_from_it).second;
                
                    for(std::list< nid_t >::iterator neighbors_to_it = neighbors_to.begin();
                            neighbors_to_it!=neighbors_to.end();
                            neighbors_to_it++)
                    {
                        nid_t nid_to = *neighbors_to_it;

                        if(nid_from!=nid_to)
                        {
                            neighbor_t neighbor;
                            neighbor.from = nid_from;
                            neighbor.to = nid_to;
                            neighbor.way_kind = way_kind;
                            neighbors.push_back(neighbor);
                        }
                    }
                }

                if(neighbors.size()==0)
                {
                    // No-exit way, last node
                    for(std::list< std::pair<nid_t,way_kind_t> >::iterator neighbors_from_it = neighbors_from.begin();
                            neighbors_from_it!=neighbors_from.end();
                            neighbors_from_it++)
                    {
                        nid_t nid_from = (*neighbors_from_it).first;
                        way_kind_t way_kind = (*neighbors_from_it).second;
                        
                        neighbor_t neighbor;
                        neighbor.from = nid_from;
                        neighbor.to = 0;
                        neighbor.way_kind = way_kind;
                        neighbors.push_back(neighbor);
                    }

                    for(std::list< nid_t >::iterator neighbors_to_it = neighbors_to.begin();
                            neighbors_to_it!=neighbors_to.end();
                            neighbors_to_it++)
                    {
                        nid_t nid_to = *neighbors_to_it;
                        
                        neighbor_t neighbor;
                        neighbor.from = 0;
                        neighbor.to = nid_to;
                        neighbor.way_kind = RV_WK_OTHERS;
                        neighbors.push_back(neighbor);
                    }
                }
            

                nodes_db.write_node(
                        nid,
                        lon,
                        lat,
                        elev,
                        neighbors);

                i++;
                if((int)(100*i/total_number_of_nodes)>previous_displayed)
                {
                    previous_displayed = 100*i/total_number_of_nodes;
                    RV_BLANKLINE;
                    fprintf(stderr,"-> Retrieving neighbors and writing nodes %i%% ",previous_displayed);
                }
            }


            
        }

        RV_BLANKLINE;
        fprintf(stderr,"-> Commit on nodes_db ");


        nodes_db.commit();
    }
        

    // lookup_db (in a scope)
    {
        RV_BLANKLINE;
        fprintf(stderr,"Writing lookup_db\n");

        RV_BLANKLINE;
        fprintf(stderr,"-> Computing the area ");
        snprintf(zreq,RV_ZCHAR_LENGTH,
                "SELECT DISTINCT floor(ST_X(geom)),floor(ST_Y(geom)) FROM %snodes",
                RV_TEMP_TABLES_PREFIX);
        pqxx::result res_area=postgres_txn.exec(zreq);

        RV_BLANKLINE;
        fprintf(stderr,"-> Lauching_queries ");
        pqxx::pipeline postgres_txn_pipeline(postgres_txn);
        postgres_txn_pipeline.retain(RV_POSTGRES_PIPELINE_RETAIN);

        std::list< std::pair<pqxx::pipeline::query_id, lookup_key_t> > queries;
        for(pqxx::result::iterator res_area_it = res_area.begin();
                res_area_it!=res_area.end();
                res_area_it++)
        {
            for(unsigned int iter_lon=0; iter_lon<RV_LOOKUP_PREC; iter_lon++)
            {
                for(unsigned int iter_lat=0; iter_lat<RV_LOOKUP_PREC; iter_lat++)
                {
                    float lon_min = res_area_it[0].as<float>() + ((float)iter_lon)/RV_LOOKUP_PREC;
                    float lat_min = res_area_it[1].as<float>() + ((float)iter_lat)/RV_LOOKUP_PREC;
                    float lon_max = lon_min + 1.0/RV_LOOKUP_PREC;
                    float lat_max = lat_min + 1.0/RV_LOOKUP_PREC;
                    snprintf(zreq,RV_ZCHAR_LENGTH,
                            "SELECT id,cc_id FROM %snodes WHERE geom && ST_MakeEnvelope(%f,%f,%f,%f)",
                            RV_TEMP_TABLES_PREFIX,
                            lon_min,
                            lat_min,
                            lon_max,
                            lat_max
                            );
                    std::pair<pqxx::pipeline::query_id, lookup_key_t> the_query;
                    the_query.first = postgres_txn_pipeline.insert(zreq);
                    the_query.second.lon = (int32_t)((180+lon_min)*RV_LOOKUP_PREC+.5);
                    the_query.second.lat = (int32_t)((180+lat_min)*RV_LOOKUP_PREC+.5);

                    queries.push_back(the_query);
                }
            }
        }

        unsigned int total_number_of_areas = queries.size();
        unsigned int i=0;
        int previous_displayed = -1;

        for(std::list< std::pair<pqxx::pipeline::query_id, lookup_key_t> >::iterator queries_it = queries.begin();
                queries_it!=queries.end();
                queries_it++)
        {
            pqxx::result res_nodes_in_area = postgres_txn_pipeline.retrieve((*queries_it).first);
            std::list<ncc_t> nodes_in_area;
            for(pqxx::result::iterator res_nodes_in_area_it = res_nodes_in_area.begin();
                    res_nodes_in_area_it!=res_nodes_in_area.end();
                    res_nodes_in_area_it++)
            {
                ncc_t ncc;
                ncc.nid = res_nodes_in_area_it[0].as<nid_t>();
                ncc.ccid = res_nodes_in_area_it[1].as<ccid_t>();

                nodes_in_area.push_back( ncc );
            }

            lookup_db.write_area((*queries_it).second,nodes_in_area);

            i++;
            if((int)(100*i/total_number_of_areas)>previous_displayed)
            {
                previous_displayed = 100*i/total_number_of_areas;
                RV_BLANKLINE;
                fprintf(stderr,"-> %i%% ",previous_displayed);
            }
        }
        RV_BLANKLINE;
        fprintf(stderr,"-> Commit on lookup_db ");


        lookup_db.commit();
    }













    RV_BLANKLINE;

    return(0);
}


    
