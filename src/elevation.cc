/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "elevation.h"

elevation::elevation(const char* db_location, bool rwi)
{
    int rc;
    rw = rwi;

    rc = mdb_env_create(&env);
    if(rc)
    {
        fprintf(stderr, "mdb_env_create: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_set_mapsize(env, RV_MAXIMUM_LMDB_SIZE);
    if(rc)
    {
        fprintf(stderr, "mdb_env_set_mapsize: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_env_open(env, db_location, rw?MDB_WRITEMAP:0, 0644);
    if(rc)
    {
        fprintf(stderr, "mdb_env_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    if(rw)
        rc = mdb_txn_begin(env, NULL, 0, &txn);
    else
        rc = mdb_txn_begin(env, NULL, MDB_RDONLY, &txn);
    if(rc)
    {
        fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    rc = mdb_dbi_open(txn, NULL, 0, &dbi);
    if(rc)
    {
        fprintf(stderr, "mdb_dbi_open: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }
}

elevation::~elevation()
{
    mdb_txn_abort(txn);
    mdb_close(env, dbi);
    mdb_env_close(env);
}

void elevation::commit()
{
    if(rw)
    {
        int rc;

        rc = mdb_txn_commit(txn);
        if(rc)
        {
            fprintf(stderr, "mdb_txn_commit: (%d) %s\n", rc, mdb_strerror(rc));
            abort();
        }

        rc = mdb_txn_begin(env, NULL, 0, &txn);
        if(rc)
        {
            fprintf(stderr, "mdb_txn_begin: (%d) %s\n", rc, mdb_strerror(rc));
            abort();
        }
    }
}

elevation_value elevation::get_elevation(double & lon, double & lat)
{
    double lon_s = lon * 3600;
    double lat_s = lat * 3600;

    int lon_s_1 = ((int)((lon_s + 1296000) / 3))*3 - 1296000;
    int lat_s_1 = ((int)((lat_s + 1296000) / 3))*3 - 1296000;

    int lon_s_2 = lon_s_1 + 3;
    int lat_s_2 = lat_s_1 + 3;

    double xlon = (lon_s - lon_s_1) / 3;
    double xlat = (lat_s - lat_s_1) / 3;

    elevation_value *pele_lon1_lat1, *pele_lon1_lat2, *pele_lon2_lat1, *pele_lon2_lat2;
    pele_lon1_lat1=grid_point(lon_s_1,lat_s_1);
    if(!pele_lon1_lat1)
    {
        fprintf(stderr,"Grid point not found, lon_s = %i, lat_s = %i\n",lon_s_1,lat_s_1);
        abort();
    }
    pele_lon1_lat2=grid_point(lon_s_1,lat_s_2);
    if(!pele_lon1_lat2)
    {
        fprintf(stderr,"Grid point not found, lon_s = %i, lat_s = %i\n",lon_s_1,lat_s_2);
        abort();
    }
    pele_lon2_lat1=grid_point(lon_s_2,lat_s_1);
    if(!pele_lon2_lat1)
    {
        fprintf(stderr,"Grid point not found, lon_s = %i, lat_s = %i\n",lon_s_2,lat_s_1);
        abort();
    }
    pele_lon2_lat2=grid_point(lon_s_2,lat_s_2);
    if(!pele_lon2_lat2)
    {
        fprintf(stderr,"Grid point not found, lon_s = %i, lat_s = %i\n",lon_s_2,lat_s_2);
        abort();
    }

    elevation_value ele_lon1_latX, ele_lon2_latX;
    ele_lon1_latX = (1-xlat) * (*pele_lon1_lat1) + xlat * (*pele_lon1_lat2);
    ele_lon2_latX = (1-xlat) * (*pele_lon2_lat1) + xlat * (*pele_lon2_lat2);

    elevation_value ele_lonX_latX;
    ele_lonX_latX = (1-xlon)*ele_lon1_latX + xlon*ele_lon2_latX;

    return(ele_lonX_latX);
}

elevation_value* elevation::grid_point(int32_t & lon_s, int32_t & lat_s, bool ptr_even_missing)
{
    // with lon_s,lat_s we determine the isblock
    int32_t lon_s_begin = ((lon_s + 3600*360)/(3*RV_ELEVATION_SQ_LEN))*RV_ELEVATION_SQ_LEN*3 - 3600*360;
    int32_t lat_s_begin = ((lat_s + 3600*360)/(3*RV_ELEVATION_SQ_LEN))*RV_ELEVATION_SQ_LEN*3 - 3600*360;

    unsigned int pos = (lon_s-lon_s_begin)/3*RV_ELEVATION_SQ_LEN+(lat_s-lat_s_begin)/3;

    MDB_val mdb_key, mdb_data;
    elevation_key key;
    
    key.lon_s = lon_s_begin;
    key.lat_s = lat_s_begin;
    mdb_key.mv_size = sizeof(elevation_key);
    mdb_key.mv_data = &key;

    int rc;
    rc = mdb_get(txn,dbi,&mdb_key,&mdb_data);
    if(rc)
    {
        if(rc == MDB_NOTFOUND)
        {
            if(rw)
            {
                elevation_value* ptr = (elevation_value*)malloc(RV_ELEVATION_SQ_LEN*RV_ELEVATION_SQ_LEN*sizeof(elevation_value));
                for(unsigned int i=0;i<RV_ELEVATION_SQ_LEN*RV_ELEVATION_SQ_LEN;i++)
                {
                    ptr[i]=-32768;
                }

                mdb_data.mv_size=RV_ELEVATION_SQ_LEN*RV_ELEVATION_SQ_LEN*sizeof(elevation_value);
                mdb_data.mv_data=ptr;
                rc = mdb_put(txn, dbi, &mdb_key, &mdb_data, 0);
                if(rc)
                {
                    fprintf(stderr, "mdb_put: (%d) %s\n", rc, mdb_strerror(rc));
                    abort();
                }

                free(ptr);

                return(grid_point(lon_s,lat_s,ptr_even_missing));
            }
            else
                return NULL;
        }
        fprintf(stderr, "mdb_get: (%d) %s\n", rc, mdb_strerror(rc));
        abort();
    }

    elevation_value* res =  ((elevation_value*)(mdb_data.mv_data)) + pos;
    if(ptr_even_missing)
        return(res);
    if(*res==-32768)
        return(NULL);
    return(res);
    
}

