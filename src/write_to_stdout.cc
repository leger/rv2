/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "write_to_stdout.h"

void write_to_stdout(
        route_db_t* route_db,
        unsigned int track,
        std::list<route_point_t> points)
{
    fprintf(stdout,"begin track %u\n",track);

    for(auto points_it = points.begin();
            points_it != points.end();
            points_it++)
    {
        node_info_t* pni = route_db->get_node(points_it->nid);

        fprintf(stdout,"nid:%lu\t",points_it->nid);
        fprintf(stdout,"lon:%.6f\t",pni->fixed->lon);
        fprintf(stdout,"lat:%.6f\t",pni->fixed->lat);
        fprintf(stdout,"elevation:%.3f\t",pni->fixed->elevation);
        fprintf(stdout,"velocity:%.3f\t",points_it->velocity);
        fprintf(stdout,"distance:%.3f\t",points_it->distance);
        fprintf(stdout,"energy:%.3f\t",points_it->energy);
        fprintf(stdout,"time:%.3f\t",points_it->time);
        fprintf(stdout,"power:%.3f\t",points_it->power);
        fprintf(stdout,"walk:%s\t",points_it->walk?"yes":"no");
        fprintf(stdout,"\n");
    }
    
    fprintf(stdout,"end track %u\n",track);

}

