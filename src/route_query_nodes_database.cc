/*
 * Copyright (c) 2012-2016, Jean-Benoist Leger <jb@leger.tf>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "nodes_db.h"
#include <iostream>

int main(int argc, char** argv)
{
    if(argc<2)
    {
        fprintf(stderr,"Usage: %s routes_db_path\nPut node id on stdin\n",argv[0]);
        abort();
    }

    // nodes_db (read)
    char zfilename[RV_ZCHAR_LENGTH];
    snprintf(zfilename,RV_ZCHAR_LENGTH,"%s/%s",argv[1],RV_NODES_DB_RELATIVE_PATH);
    nodes_db_t nodes_db(zfilename);

    while(!std::cin.eof())
    {
        nid_t nid;
        std::cin >> nid;
        if(std::cin.eof())
            break;

        node_info_t ni;
        
        nodes_db.get_node(nid,&ni);
        printf("Node (%lu):\n",nid);
        printf("    -> lon: %f\n", ni.fixed->lon);
        printf("    -> lat: %f\n", ni.fixed->lat);
        printf("    -> ele: %f\n", ni.fixed->elevation);
        printf("    -> neighbors: %u\n", ni.fixed->neighbors_number);

        for(unsigned int i=0; i<ni.fixed->neighbors_number; i++)
        {
            printf("        -> f:%lu t:%lu wk:%i\n",
                    ni.neighbors[i].from,
                    ni.neighbors[i].to,
                    ni.neighbors[i].way_kind);
        }
    }

    return(0);
}



