#!/bin/bash

# Copyright (c) 2012-2014, Jean-Benoist Leger <jb@leger.tf>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

if [[ $# -lt 2 ]]
then
    echo "Usage: $0 list_needed_srtm_tiles srtmzip/" >&2
    exit 1
fi

directory="$2/"
list_needed_tiles=$1
baseurls="http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Africa
http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Australia
http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia
http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Islands
http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/North_America
http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/South_America"

mkdir -p $directory

cat $list_needed_tiles | while read one_line
do
    lon=$(printf -- "$one_line" | cut -f 1)
    lat=$(printf -- "$one_line" | cut -f 2)

    if [[ $lat -ge 0 ]] # 0 is N
    then
        strlat=$( printf "N%02d" $lat )
    else
        strlat=$( printf "S%02d" $((-$lat)) )
    fi

    if [[ $lon -ge 0 ]] # 0 is E
    then
        strlon=$( printf "E%03d" $lon )
    else
        strlon=$( printf "W%03d" $((-$lon)) )
    fi

    name=$strlat$strlon.hgt
    for baseurl in $baseurls
    do
        printf "$baseurl/$name.zip\n"
    done
done | wget -c -i - -P "$directory"


