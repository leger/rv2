First
=====

You must have a route_db.

Input
=====

Parameters
----------

The `get_route` program read parameters and route point in stdin. Each parameter
is given in a line, the parameter name separated from the value by a single
space.


Each non-given parameter is used as its default value. See
`./src/read_from_stdin.cc`.

Available parameters are:

 - `mass` (in kg): the total mass (bicycle + human) (default: 90)

 - `velocity_base` (in m.s^-1): the base velocity on flat road (default: 6.95)

 - `SCx` (in m^2): the equivalent surface for wind (default: .45)

 - `Cr`: the resistance of the road (default: .008)

 - `velocity_nopower` (in m.s^-1): when we stop giving power (default: 9.7)
 
 - `velocity_brake` (in m.s^-1): when we start to brake (default: 13.9)

 - `velocity_equilibrium` (in m.s^-1): when we walk (default: 1.4)

 - `power_walk` (in W): power used for walk (default: 140)

 - `lateral_acceleration` (in `g`. `g=9.81m.s^-2`): maximum lateral acceleration (default: .5)

 - `walk_penalty`: multiplicative penalty for walking (default: 1, meaning no penalty)

 - `Cw`: resistance of road for walking (default: .03)

 - `criterion`: (default: `energy`) valid values are:

  - `energy`: the total energy is minimized

  - `energy_d`: the total energy is minimzed using Dijkstra algorithm. Only for
    test. This must not be availble to final users.

  - `time`: the total time is minimized

  - `time_d`: the total time is minimzed using Dijkstra algorithm. Only for
    test. This must not be availble to final users.
  
  - `distance`: the total distance is minimized

Help choosing parameters
------------------------

To choose the `power_walk` parameter, choosing the same value as `power` is a
good idea.

To choose `lateral_acceleration`. The first for a road with width 2.5m, the
curvature radius of a perpendicular turn is `Rc=8.5m`. Let `velocity` the speed
in a turn with this curvature radius:

    lateral_acceleration (in m.s^-2) = velocity^2 / Rc

Then

    lateral_acceleration (in g) = [lateral_acceleration (in m.s^-2)] / g

Routing points
--------------

Before starting giving routing points. The line `points` must be given on stdin.

Each line is a point, lon and lat are given (in this order) separated by a
space.

Starting
--------

To start the routing, the line `route` must be given on stdin.

Example of input
----------------

    mass 112.0
    criterion energy
    SCx .40
    walk_penalty 2
    points
    0.044 45.661
    0.169 45.648
    0.240 45.737
    0.335 46.599
    route

stderr
======

The program give on stderr two type do message. One message per line.

state
-----

This line is formated as following

    ## state: <state name> [args]

`<state name>` can be

 - `failed`. `args` explain why.

 - `lookup`.

 - `routing_track`. The `args` contain the track id.

status
------

This line is formated as following

    ## status: <integer>

The status indicate where the algorithm is for the current track routing process
(see `routing_track` state message).

The integer is between `0` and `RV_STATUS_SCALE` (1000). Warning: the status is
always increasing inside a track routing, but not by steps of 1.

stdout
======

Results are given on `stdout`. For each track, the output is the following:

    begin track <track id>
    <point>
    ...
    <point>
    end track <track id>

Each point is described as a line with the following formating:

    <key>:<value> <key>:<value> ... <key>:<value>

The keys are:

 - `nid`: the node id

 - `lon`: the longitude of the node

 - `lat`: the latitude of the node

 - `elevation`: the elevation of the node

 - `velocity`: the instant velocity (in m.s^-1) on this node

 - `distance`: the distance (in m) since the begining of the track

 - `energy`: the used energy (in J) since the begining of the track

 - `time`: the time (in s) since the begining of the track

 - `power`: the mean power (in W) on the edge between previous node and this one.

 - `walk`: `yes` or `no` indicate if we walk from the previous node.

Warning: cumulated values (`distance`, `energy`, and `time`) are reinitalized to
0 at the beginning of each track.

Note also each track is printed as soon as possible.

Example
=======

For a trip in Poitou-Charentes (France). I see stderr, and I write stdout in a
file.

    echo \
    "mass 112.0
    criterion energy
    SCx .40
    walk_penalty 2
    points
    0.044 45.661
    0.169 45.648
    0.240 45.737
    0.335 46.599
    route" \
      | ./src/get_route SOMEWHERE/route_db > /tmp/pcroute.txt

After I look the file (-S to don't break line).

    less -S /tmp/pcroute.txt

License of this file
====================

    Copyright (c) 2015-2016, Jean-Benoist Leger <jb@leger.tf>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

