Librairies
==========

LMDB
----

This lib is necessary for all executables. Let your distribution do the job.
(liblmdb-dev on debian).

osmpbf
------

This lib is necessary only for the creation of route database. Do not do this in
the rv2 dir but in the dir which contains rv2 (or change rv2 src makefile).

This lib need libprotobuf to be build and the protobuf compiler. Let your
distribution do the job (libprotobuf-dev and protobuf-compiler on debian).

    git clone https://github.com/inphos42/osmpbf.git
    cd osmpbf
    git clone https://github.com/inphos42/generics.git
    cmake .
    make

Build
=====

Go in the src dir, and use make for the target you want. The following target
are available:

 - `elevation_create_database`: creation of elevation database from srtm files
 
 - `elevation_query_database`: query of elevation by coordinates
 
 - `lmdb_rewriter`: rewrite a lmdb database, usefull to rewrite big sparse file as
   little dense one.

 - `route_create_database_from_pbf`: (needs osmpbf lib) creation of the route
   database (constituted by two db, lookup and nodes) from a pbf file and using
   elevation database.

 - `route_query_lookup_database`: query lookup db in route db to found nodes id
   near the given coordinates. Nodes are searched on the same connected
   component.

 - `route_query_nodes_database`: query nodes db in route db to have informations
   about given nodes id.

 - `get_route`: Give a route between given coordinated. The goal of the project.

License of this file
====================

    Copyright (c) 2015-2016, Jean-Benoist Leger <jb@leger.tf>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.

